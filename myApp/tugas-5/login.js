import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput } from 'react-native';
import { Constants } from 'expo';
import images from './assets/images.png';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  render() {
    return (
      <View style={{ 
        height: 100 + '%',
        width: 100 + '%',
        backgroundColor: 'rgb(39, 66, 16)',
        justifyContent: 'center',
        alignItems: 'center',

      }}>

        <Text style = 
        {{
          fontSize: 30,
          color: 'rgb(136, 114, 19)',
          
     
        }}
        >Welcome 
        </Text>
        

   <Image 
   source = {images}
    style = {{ 
      height: 61,
      width: 90,
      marginTop: 20,
      resizeMode: 'contain'
      }} 
   />
      <TextInput
      placeholder = "Username"
      placeholderTextColor = "white"
      style = {{
      backgroundColor: 'rgba(204, 204, 204, 0.5)',
      width: 80 + '%',
      padding: 10,
      borderRadius: 30,
      marginTop: 40,

      }}
       />
          <TextInput
      placeholder = "Password"
      placeholderTextColor = "white"
      style = {{
      backgroundColor: 'rgba(204, 204, 204, 0.5)',
      width: 80 + '%',
      padding: 10,
      borderRadius: 30,
      marginTop: 18,

      }}

    
       />
        <Text style = 
        {{
          backgroundColor: 'rgba(136, 114, 19, 0.9)',
      width: 20 + '%',
      padding: 10,
      borderRadius: 30,
      marginTop: 30,
          color: 'white',
          
     
        }}
        >Sign In</Text>
      </View>
    );
  }
}


