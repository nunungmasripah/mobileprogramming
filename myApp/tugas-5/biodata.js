import * as React from 'react';
import { Text, View, StyleSheet, Image, TextInput } from 'react-native';
import { Constants } from 'expo';
import images from './assets/nunung.jpg';

// You can import from local files
import AssetExample from './components/AssetExample';

// or any pure javascript modules available in npm
import { Card } from 'react-native-paper';

export default class App extends React.Component {
  render() {
    return (
      <View style={{ 
        height: 100 + '%',
        width: 100 + '%',
        backgroundColor: 'rgb(39, 66, 16)',
        justifyContent: 'center',
        alignItems: 'center',

      }}>

        <Text style = 
        {{
          fontSize: 30,
          fontWeight: 'bold',
          color: 'rgb(136, 114, 19)'
        }}
        >Biodata
        </Text>
        

   <Image 
   source = {images}
    style = {{ 
      height: 90,
      width: 90,
      borderRadius: 44,
      marginTop: 20
      }} 
   />
        <Text style = 
        {{
        marginTop: 29,
        fontSize: 22,
        color: 'rgb(136, 114, 19)'
          
        }}
        >Nunung Masripah</Text>

        <Text style = 
        {{
        marginTop: 20,
        fontSize: 20,
        bottom: -36,
        right: 107,
        color: 'black'
        }} 
        >Program Studi</Text>

         <Text style = 
        {{
        marginTop: 20,
        fontSize: 20,
        bottom: -20,
        right: 90,
        color: 'rgb(136, 114, 19)'
        }} 
        >Teknik Informatika</Text>

        <Text style = 
        {{
        marginTop: 20,
        fontSize: 20,
        bottom: -36,
        right: 136,
        color: 'black'
        }} 
        >Kelas</Text>

         <Text style = 
        {{
        marginTop: 20,
        fontSize: 20,
        bottom: -20,
        right: 132,
        color: 'rgb(136, 114, 19)'
        }} 
        >Pagi C</Text>

        <Text style = 
        {{
        marginTop: 20,
        fontSize: 20,
        bottom: -36,
        right: 117,
        color: 'black'
        }} 
        >Instagram</Text>

         <Text style = 
        {{
        marginTop: 20,
        fontSize: 20,
        bottom: -20,
        right: 120,
        color: 'rgb(136, 114, 19)'
        }} 
        >@nu2ng_</Text>

        
      </View>
    );
  }
}


